/**
 *  Bài tập 1 - Tính lương nhân viên
 *
 * Dữ liệu đầu vào: Lương 1 ngày 100 ngàn
 *
 * Các bước xử lý:
 *      -Khai báo lương 1 ngày = 100
 *      -Cho người dùng nhập vào số ngày công
 *      -Tính lương dựa vào số ngày công
 *      -Xuất ra kết quả
 *
 * Dữ liệu đầu ra: Tổng lương là 2600
 */
function tinhTien() {
  var luongMotNgay = 100;
  var soNgayCong = document.getElementById("txt-so-ngay-cong").value * 1;
  var tienLuong = luongMotNgay * soNgayCong;
  document.getElementById(
    "result"
  ).innerHTML = `<p> Tiền lương tháng này là ${tienLuong} USD`;
}

/**
 * Bài tập 2 - Tính giá trị trung bình
 *
 * Dữ liệu đầu vào: người dùng nhập vào 5 số lần lượt là
 * 25 - 64 - 74 - 39 - 86
 *
 * Các bước xử lý:
 *      -Khai báo lần lượt 5 số được nhập vào
 *      -Tính trung bình cộng 5 số
 *      -Xuất ra kết quả
 *
 * Dữ liệu đầu ra: Giá trị trung bình là 57.6
 */
var a = 25;
var b = 64;
var c = 74;
var d = 39;
var e = 86;
var AVG = (a + b + c + d + e) / 5;
console.log("Giá trị trung bình: ", AVG);

/**
 * Bài tập 3 - Quy đổi tiền
 *
 * Dữ liệu đầu vào: giá USD hiện nay là 23.500 VND,
 * người dùng nhập vào số tiền cần quy đổi là 3000USD
 *
 * Các bước xử lý:
 *      -Khai báo giá quy đổi 1USD sang VND
 *      -Khai báo số tiền cần quy đổi
 *      -Tính giá trị số tiền sau quy đổi
 *      -Xuất ra kết quả
 *
 * Dữ liệu đầu ra: 70.500.000VND
 */
var giaUSD = 23500;
var quyDoi = 3000 * giaUSD;
console.log("Số tiền sau khi quy đổi: ", quyDoi, "VND");

/**
 * Bài tập 4 - Tính diện tích, chu vi hình chữ nhật
 *
 * Dữ liệu đầu vào: Diện tích = dài * rộng
 *                  Chu vu =  ( dài + rộng ) * 2
 * Người dùng nhập vào chiều dài bằng 4 và chiều rộng bằng 2
 *
 * Các bước xử lý:
 *      -Khai báo chiều dài và chiều rộng
 *      -Tính diện tích và chu vi theo công thức
 *      -Xuất ra kết quả
 *
 * Dữ liệu đầu ra: Diện tích = 8
 *                 Chu vi = 12
 */
var chieuDai = 4;
var chieuRong = 2;
var dienTich = chieuDai * chieuRong;
var chuVi = (chieuDai + chieuRong) * 2;
console.log("Diện tích hình chữ nhật: ", dienTich);
console.log("Chu vi hình chữ nhật: ", chuVi);

/**
 * Bài tập 5 - Tính tổng 2 ký số
 *
 * Dữ liệu đầu vào: Cho 1 số có 2 ký số là 97
 *
 * Các bước xử lý:
 *      -Khai báo ký số
 *      -Thực hiện tính toán để tách số hàng chục và hàng đơn vị
 *      -Tính tổng 2 ký số
 *      -Xuất ra kết quả
 *
 * Dữ liệu đầu ra: 16
 */
var kySo = 97;
var hangDonVi = kySo % 10;
var hangChuc = Math.floor(kySo / 10);
var tong = hangChuc + hangDonVi;
console.log("Tổng 2 ký số: ", tong);
