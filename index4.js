/**
 *  Bài tập 1
 *
 * dữ liệu đầu vào: cho người dùng nhập 3 số bất kỳ
 *
 * các bước thực hiện: xử dụng else if để sàng lọc ra các trường hợp
 *
 * kết quả: xuất ra 3 số người dùng vừa nhập theo thứ tự tăng dần
 */
function sapXep() {
  var soThuNhat = document.getElementById("txt-so-thu-nhat").value * 1;
  var soThuHai = document.getElementById("txt-so-thu-hai").value * 1;
  var soThuBa = document.getElementById("txt-so-thu-ba").value * 1;

  if (soThuNhat < soThuHai && soThuHai < soThuBa) {
    document.getElementById(
      "result"
    ).innerHTML = `${soThuNhat} < ${soThuHai} < ${soThuBa}`;
  } else if (soThuNhat < soThuHai && soThuBa < soThuNhat) {
    document.getElementById(
      "result"
    ).innerHTML = `${soThuBa} < ${soThuNhat} < ${soThuHai}`;
  } else if (soThuNhat > soThuHai && soThuHai > soThuBa) {
    document.getElementById(
      "result"
    ).innerHTML = `${soThuBa} < ${soThuHai} < ${soThuNhat}`;
  } else if (soThuNhat < soThuBa && soThuBa < soThuHai) {
    document.getElementById(
      "result"
    ).innerHTML = `${soThuNhat} < ${soThuBa} < ${soThuHai}`;
  } else if (soThuHai < soThuBa && soThuBa < soThuNhat) {
    document.getElementById(
      "result"
    ).innerHTML = `${soThuHai} < ${soThuBa} < ${soThuNhat}`;
  } else {
    document.getElementById("result").innerHTML = `Trường hợp này chưa nghĩ ra`;
  }
}

/**
 *  Bài tập 2
 *
 * Tham số đầu vào: Cho người dùng chọn đối tượng muốn chào
 *
 * Các bước xử lý: xử dụng select option để chọn người dùng và xuất ra lời chào
 *
 * Kết quả: khi người dùng chọn người nào thì sẽ hiện ra lời chào người đó
 */
function guiLoiChao() {
  var e = document.getElementById("ddlViewBy");
  var value = e.value;
  var text = e.options[e.selectedIndex].text;
  document.getElementById("result_2").innerHTML = `Xin chao ${text}`;
}

/**
 * Bài 3
 *
 * Dữ liệu đầu vào: cho người dùng nhập 3 số bất kỳ
 *
 * Các bước tính toán: Xử dụng if và % để loại ra số chẵn và số lẻ
 *
 * Kết quả: Xuất ra có bao nhiêu số chẵn và lẻ
 *
 */
function sapXepChanLe() {
  var soMot = document.getElementById("txt-so-mot").value;
  var soHai = document.getElementById("txt-so-hai").value;
  var soBa = document.getElementById("txt-so-ba").value;
  var count = 0;
  if (soMot % 2 == 0) {
    count++;
  } else {
    count + 0;
  }
  if (soHai % 2 == 0) {
    count++;
  } else {
    count + 0;
  }
  if (soBa % 2 == 0) {
    count++;
  } else {
    count + 0;
  }
  document.getElementById(
    "result_3"
  ).innerHTML = `<p> Số lượng số chẵn là: ${count} </br> Số lượng số lẻ là: ${
    3 - count
  } </p>`;
}

/**
 * Bài 4
 */
function tamGiac() {
  var canhThuNhat = document.getElementById("txt-canh-mot").value;
  var canhThuHai = document.getElementById("txt-canh-hai").value;
  var canhThuBa = document.getElementById("txt-canh-ba").value;
  if (canhThuNhat == canhThuHai && canhThuHai == canhThuBa) {
    document.getElementById(
      "result_4"
    ).innerHTML = `<p> Đây là tam giác đều</p>`;
  } else if (
    (canhThuNhat == canhThuHai && canhThuHai !== canhThuBa) ||
    (canhThuNhat == canhThuBa && canhThuBa !== canhThuHai) ||
    (canhThuBa == canhThuHai && canhThuHai !== canhThuNhat)
  ) {
    document.getElementById(
      "result_4"
    ).innerHTML = `<p> Đây là tam giác cân </p>`;
  } else if (
    canhThuNhat * canhThuNhat ==
      canhThuHai * canhThuHai + canhThuBa * canhThuBa ||
    canhThuHai * canhThuHai ==
      canhThuNhat * canhThuNhat + canhThuBa * canhThuBa ||
    canhThuBa * canhThuBa == canhThuNhat * canhThuNhat + canhThuHai * canhThuHai
  ) {
    document.getElementById(
      "result_4"
    ).innerHTML = `<p> Đây là tam giác vuông </p>`;
  } else {
    document.getElementById(
      "result_4"
    ).innerHTML = `<p> Đây là tam giác thường </p>`;
  }
}
